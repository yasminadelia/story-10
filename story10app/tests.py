from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .views import *

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time



# Create your tests here.
class UnitTest(TestCase):
	
	def test_index_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_calling_right_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)

	def test_page_is_not_exist(self):
		self.response = Client().get('/NotExistPage/')
		self.assertEqual(self.response.status_code, 404)

# class Story10AppFunctionalTest(StaticLiveServerTestCase):

# 	def setUp(self):
		
# 		chrome_options = Options()
# 		chrome_options.add_argument("--disable-dev-shm-usage")
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')        
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')

		
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		super(Story10AppFunctionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(Story10AppFunctionalTest, self).tearDown()

# 	def test_8000(self):
# 		selenium = self.selenium
#         # Opening the link we want to test
# 		selenium.get('http://localhost:8000/')
# 		time.sleep(5) 

		
# 		result = selenium.find_element_by_id('host-name')
# 		self.assertIn('8000', result.get_attribute('innerHTML').lower())

# 	def test_9000(self):
# 		selenium = self.selenium
#         # Opening the link we want to test
# 		selenium.get('http://localhost:9000/')
# 		time.sleep(5) 

		
# 		result = selenium.find_element_by_id('host-name')
# 		self.assertIn('9000', result.get_attribute('innerHTML').lower())

		